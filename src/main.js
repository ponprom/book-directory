import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import StoragePlugin from "vue-web-storage";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  axios,
  StoragePlugin,
  render: (h) => h(App),
}).$mount("#app");
