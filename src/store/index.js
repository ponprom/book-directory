import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    book: null,
    lastRoute: null,
    books: [],
    favoriteList: [],
  },
  mutations: {
    SET_ROUTE(state, lastRoute) {
      state.lastRoute = lastRoute;
    },
    SET_BOOK(state, book) {
      state.book = book;
    },
    SET_BOOKS(state, books) {
      state.books = books;
    },
    SET_FAVORITES(state, favoriteList) {
      state.favoriteList = favoriteList;
    },
    ADD_FAVORITE(state, favorite) {
      state.favoriteList.push(favorite);
    },
    REMOVE_FAVORITE(state, id) {
      state.favoriteList.splice(
        state.favoriteList.findIndex((a) => a.id === id),
        1
      );
    },
  },
  actions: {
    fetchRoute(context, route) {
      context.commit("SET_ROUTE", route);
    },
    async fetchBooks(context, params) {
      await axios
        .get("https://www.googleapis.com/books/v1/volumes", {
          params,
        })
        .then((response) => {
          context.commit("SET_BOOKS", response.data.items);
        });
    },
    async fetchBook(context, id) {
      await axios
        .get(`https://www.googleapis.com/books/v1/volumes/${id}`)
        .then((response) => {
          context.commit("SET_BOOK", response.data);
        });
    },
    fetchFavoriteList(context) {
      if (localStorage.getItem("favorite-list")) {
        try {
          const fav = JSON.parse(localStorage.getItem("favorite-list"));
          context.commit("SET_FAVORITES", fav);
        } catch (e) {
          localStorage.removeItem("favorite-list");
        }
      }
    },
    addFavoriteList(context, favoriteBook) {
      context.commit("ADD_FAVORITE", favoriteBook);
      let parsed = JSON.stringify(this.state.favoriteList);
      localStorage.setItem("favorite-list", parsed);
    },
    removeFavoriteBook(context, id) {
      context.commit("REMOVE_FAVORITE", id);
      let parsed = JSON.stringify(this.state.favoriteList);
      localStorage.setItem("favorite-list", parsed);
    },
  },
  getters: {
    getBooks() {
      return this.state.books;
    },
    getFavoriteList() {
      return this.state.favoriteList;
    },
  },
  modules: {},
});
